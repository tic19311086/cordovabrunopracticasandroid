package com.brunocordovaelbuapo.practica3plantilla;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class IntentoActivity extends AppCompatActivity {
    List<ListElement> elements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intento);
        init();
    }

    public void init() {
        elements = new ArrayList<>();
        elements.add(new ListElement("#775447","Bruno","Hermosillo","Activo"));
        elements.add(new ListElement("#548698","Juan","Guadalajara","Inactivo"));
        elements.add(new ListElement("#253645","Luis","Mexico","Activo"));
        elements.add(new ListElement("#568956","Guadalupe","Chihuhua","Inactivo"));
        elements.add(new ListElement("#316589","Sofia","Tijuana","Activo"));
        elements.add(new ListElement("#753958","Andrea","Juarez","Activo"));
        elements.add(new ListElement("#254787","Saray","Phoenix","Activo"));
        elements.add(new ListElement("#789456","Elena","Monterrey","Activo"));
        elements.add(new ListElement("#654789","Ayleen","Lima","Activo"));
        elements.add(new ListElement("#123375","Ramon","Sacramento","Activo"));
        elements.add(new ListElement("#986547","Victor","Hermosillo","Activo"));

        ListAdapter listAdapter = new ListAdapter(elements, this);
        RecyclerView recyclerView = findViewById(R.id.listRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }

}

